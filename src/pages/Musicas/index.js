import React, { Component } from 'react'
import { Text, View, SafeAreaView, FlatList, TouchableOpacity, Platform } from 'react-native'

import { api, ioURL } from '../../services/api'

import socket from 'socket.io-client'
import styles from './styles'

import Icon from 'react-native-vector-icons/MaterialIcons'

class Musicas extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Musicas'
    })

    _isMounted = false;

    state = {
        registros: []
    }

    async componentDidMount() {
        this.subscribeToEvents();

        const res = await api.get('/musicas')
        let registros = this.ordenar(res.data.docs)
        this._isMounted = true;

        if (this._isMounted) {
            console.log(registros)
            this.setState({
                registros
            })
        }
    }

    ordenar = (colDados) => {
        return colDados.sort(function (item1, item2) {
            return item2['score'] - item1['score']
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    subscribeToEvents = () => {
        const io = socket(ioURL)

        io.on('editMusica', data => {
            let registros = [...this.state.registros.filter(item => { return item._id !== data._id }), data]
            registros = this.ordenar(registros)

            this.setState({
                registros
            })
        })
    }

    handleDiminuir = async (i) => {

        // Decrementa um ponto
        i.score = i.score - 1;

        const res = await api.put('/musicas/' + i._id, i)

        let registros = [...this.state.registros.filter(item => { return item._id !== i._id }), res.data]
        registros = this.ordenar(registros)
        console.log(registros)

        this.setState({
            registros
        })

    }

    handleAumentar = async (i) => {

        // Incrementa um ponto
        i.score = i.score + 1;

        const res = await api.put('/musicas/' + i._id, i)

        let registros = [...this.state.registros.filter(item => { return item._id !== i._id }), res.data]
        registros = this.ordenar(registros)
        console.log(registros)

        this.setState({
            registros
        })

    }

    renderItem = (item) => {
        return (
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                <Text style={styles.itemText}>{item.score} - {item.titulo}</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity style={styles.item} onPress={() => this.handleDiminuir(item)}>
                        <Icon name="arrow-downward" size={24} color="darkred" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={() => this.handleAumentar(item)}>
                        <Icon name="arrow-upward" size={24} color="darkgreen" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList data={this.state.registros} keyExtractor={(t) => t._id} renderItem={({ item }) => this.renderItem(item)} />
            </SafeAreaView>
        )
    }
}

export default Musicas