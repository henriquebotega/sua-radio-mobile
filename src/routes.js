import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'

import Musicas from './pages/Musicas'

const Routes = createAppContainer(
    createSwitchNavigator({
        Musicas,
    })
)

export default Routes